import React, { Component } from 'react';

class Card extends Component {
  render() {
    const {heading} = this.props;
    return (
      <div className="divider">
          <h2>{heading}</h2>
      </div>
    );
  }
}
export default Card;