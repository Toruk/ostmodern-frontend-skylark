import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk'
import promise from "redux-promise-middleware";
import { createLogger } from 'redux-logger';

// import scss

import reducer from './reducers';
import App from './containers/App';

const store = createStore(
    reducer,
    applyMiddleware(promise(),  thunk,  createLogger())
);

ReactDOM.render(
  <Provider store={store}>
        <App />
  </Provider>
  , document.querySelector('.app'));
