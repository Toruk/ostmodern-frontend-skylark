import { combineReducers } from 'redux';
import set from './reducer_set';
import episodes from './reducer_episodes';

const rootReducer = combineReducers({
    set,
    episodes
});

export default rootReducer;
