import { FETCH_SETS, GET_EPISODES } from '../constants/ActionTypes';

const initialState = {
    uid: undefined,
    title: undefined,
    items: [],
    body: undefined,
    summary: undefined,
    created: null,
    modified: null,
    image_urls: [],
};

export default function set(state = initialState, action) {
    switch (action.type) {
        case FETCH_SETS:
            return Object.assign({}, state, {
                uid: action.data.uid,
                title: action.data.title,
                items: action.data.items,
                body: action.data.body,
                summary: action.data.summary,
                created: action.data.created,
                modified: action.data.modified,
                image_urls: action.data.image_urls
            });
            break;
        default:
            return state;
    }
}