export const API_ENDPOINT_SETS = 'http://feature-code-test.skylark-cms.qa.aws.ostmodern.co.uk:8000/api/sets/';

// Episode End points
export const API_EPISODE_ENDPOINT = 'http://feature-code-test.skylark-cms.qa.aws.ostmodern.co.uk:8000/api/episodes/';

export const API_ENDPOINT = 'http://feature-code-test.skylark-cms.qa.aws.ostmodern.co.uk:8000';