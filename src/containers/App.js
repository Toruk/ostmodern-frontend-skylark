import React, { Component } from 'react'
import { Router, Route, browserHistory } from 'react-router'

import Sets from './Sets';
import Episode from './Episode';

class App extends Component {
    render() {
        return (
            <Router history={browserHistory}>
                <Route exact path='/' component={Sets} />
                <Route path='/set/:uid' component={Sets} />
                <Route path='/episode/:uid' component={Episode} />
                <Route render={() => <h1>Page not found</h1>} />
            </Router>
        )
    }
}
export default App