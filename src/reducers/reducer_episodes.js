import { FETCH_SET_EPISODES, FETCH_SELECTED_EPISODE } from '../constants/ActionTypes';

const initialState = {
    setEpisodes: undefined,
    selectedEpisode: undefined
};

export default function episodes(state = initialState, action) {
    switch (action.type) {
        case FETCH_SET_EPISODES:
            return Object.assign({}, state, {
                setEpisodes: action.state.data
            });
            break;
        case FETCH_SELECTED_EPISODE:
            return Object.assign({}, state, {
                selectedEpisode: action.data
            });
            break;
        default:
            return state;
    }
}