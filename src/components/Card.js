import React, { Component } from 'react';
import Moment from 'moment';
import { Link } from 'react-router';

class Card extends Component {
    render() {
        const { uid, created, title } = this.props;
        Moment.locale('en');

        return (
            <div className="card clearfix">
                <div className="card_body">
                    <h2>{title}</h2>
                    <span>Created: {Moment(created).format('d MMM')}</span>
                </div>
                <Link to={'/episode/' + uid}>More Info</Link>
            </div>
        );
    }
}
export default Card;