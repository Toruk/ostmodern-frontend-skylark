import React, { Component } from 'react';
import { Link } from 'react-router';
import Moment from 'moment';

class Info extends Component {
    render() {
        const {data} = this.props;
        Moment.locale('en');

        return (
            <div className="info">
                <div className="info__media">
                    {/* Would add the episode image but image in api is empty */}
                    <img src="https://unsplash.it/480/400" alt=""/>
                </div>
                <div className="info__body">
                    <h1>{data.title}</h1>
                    <p><b>Published on:</b> {Moment(data.publish_on).format('d MMM')}</p>
                    <p><b>Created on:</b> {Moment(data.created).format('d MMM')}</p>
                    <h4>Synopsis:</h4>
                    <p>{!!(data.synopsis)?data.synopsis:"No synopsis given"}</p>
                    <Link to="/">Return to Home</Link>
                </div>
            </div>
        );
    }
}
export default Info;