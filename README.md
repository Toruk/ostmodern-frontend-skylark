# OST Modern FrontEnd Test


### Setup

```
> install chrome extention for CORS (see link below)
> npm install (if using mac os may need to use sudo)
> npm start
```


### Notes

- Had issues with CORS whilst using the skylark api. It doesn't seem to have the correct origin header (Access-Control-Allow-Origin:*). Had to use a chrome extension to help with this issue. https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi

- The Skylark API seems to be missing a lot of content eg synopsis, images, etc. So app does look a bit bare.
