import axios from 'axios';
import { FETCH_SETS, FETCH_SET_EPISODES, FETCH_SELECTED_EPISODE } from '../constants/ActionTypes';
import { API_ENDPOINT_SETS, API_EPISODE_ENDPOINT, API_ENDPOINT } from '../constants/apiEndpoint';

// Fetch the Set from the SKYLARK API

export function fetchSet(uid) {
    return dispatch => {
        const url = API_ENDPOINT_SETS + uid + '/';
        axios.get(url)
            .catch(err => {
                console.error(err);
            })
            .then(req => {
                const data = req.data;
                dispatch(applySet(data));
            })
    }
}

function applySet(data) {
    return {
        type: FETCH_SETS,
        data
    }
}

// Fetch all the episodes from the set

export function fetchEpisodes(episodes) {
    return dispatch => {
        let promises = [];
        let results = [];
        // Loop through each episode in list
        episodes.map(i => {
            const {content_type, content_url} = i;
            if (content_type === 'episode' && content_url !== undefined) {
                const url = API_ENDPOINT + content_url;
                promises.push(axios.get(url));
            }
        });

        axios.all(promises)
            .then(req => {
                req.map(i => {
                    results.push(i.data);
                })
            })
            .then(() => {
                dispatch(applyEpisodes(results));
            });

    }
}

function applyEpisodes(data) {
    return {
        type: FETCH_SET_EPISODES,
        state: {
            data: data,
        }
    }
}

// Fetch Individual Episode data

export function fetchEpisode(uid) {
    return dispatch => {
        const url = API_EPISODE_ENDPOINT + uid + '/';
        axios.get(url)
            .then(req => {
                dispatch(applyEpisode(req.data))
            })
    }
}

function applyEpisode(data) {
    return {
        type: FETCH_SELECTED_EPISODE,
        data
    }
}