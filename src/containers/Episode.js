import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchEpisode } from '../actions/actionCreator';
import Info from "../components/Info";

class Episode extends Component {
    componentWillMount() {
        const {dispatch} = this.props;
        const {uid} = this.props.params;
        dispatch(fetchEpisode(uid));
    }


    render() {
        // Check if selected episode has data
        if (this.props.episodes.selectedEpisode) {
            const data = this.props.episodes.selectedEpisode;
            return <Info data={data} />;
        }else{
            return <h2>No Episode was found</h2>
        }
    }
}

function mapStateToProps(state) {
    const { episodes } = state;
    return {
        episodes
    }
}

export default connect(mapStateToProps)(Episode);