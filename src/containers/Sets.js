import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchSet, fetchEpisodes } from '../actions/actionCreator';

// Import components
import Card from './../components/Card';
import Divider from './../components/Divider';
import Loader from './../components/Loader';

class Sets extends Component {
    componentWillMount() {
        const { dispatch } = this.props;
        const { uid } = this.props.params;
        // Request the Set from SKYLARK API
        // Check if a uid has been passed through the url
        if (uid) {
            dispatch(fetchSet(uid));
        }else{
            // Show Home set as default
            dispatch(fetchSet('coll_e8400ca3aebb4f70baf74a81aefd5a78'));
        }
    }

    renderEpisodes() {
        const { dispatch, episodes, set } = this.props;
        if (set.items.length > 0 && episodes.setEpisodes === undefined) {
            dispatch(fetchEpisodes(set.items));
        }
        if (episodes.setEpisodes !== undefined) {
            return episodes.setEpisodes.map(i => {
                return <Card key={i.uid} uid={i.uid} title={i.title} created={i.created} />
            });
        }else{
            if (set.items.length > 0) {
                return <Loader />
            }else{
                return <Divider heading={"No Episodes found"} />
            }

        }
    }

    render() {
        const {set} = this.props;
        return (
            <div className="container">
                <Divider heading={"Contents of " + set.title}/>
                {this.renderEpisodes()}
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { set, episodes } = state;
    return {
        set,
        episodes
    }
}

export default connect(mapStateToProps)(Sets);